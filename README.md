# yesdok frontend web test - by Andika Nugraha

## Explanation

Pada program web ini saya menggunakan framework NuxtJS dengan CSS Framework TailwindCSS. Untuk data artikel saya simpan dalam file JSON di folder ~/data. Data artikel tersebut kemudian diakses oleh Vuex store, sehingga bisa diakses secara global. Untuk tampilan web ini masih cukup sederhana sehingga tidak memerlukan kostumisasi CSS tambahan, hanya menggunakan TailwindCSS. Plugin yang terpasang pada web ini yaitu PWA (Progressive Web App), AMP (Accelerated Mobile Pages). Untuk SEO, setiap artikel menggunakan slug sehingga URL friendly, dan header setiap artikel juga telah mengikuti judul artikel, juga ada tambahan meta keyword dan description di setiap page untuk meningkatan kualitas SEO. Untuk style coding di sini saya menggunakan Eslint dan Prettier, dengan mengaktifkan auto format saat menyimpan file. IDE yang digunakan yaitu VSCode. Sehingga coding terstruktur dengan rapi.

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

## Special Directories

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

### `assets`

The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).

### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

### `store`

This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).
