import articles from '~/data/articles.json'

export const state = () => ({
  articles,
})

export const getters = {
  find: (state) => (slug) => {
    return state.articles.find((o) => o.slug === slug)
  },
  findCategory: (state) => (category) => {
    return state.articles.filter((o) => o.category === category)
  },
  categories: (state) => {
    const categories = state.articles.map((item) => item.category)
    return [...new Set(categories)]
  },
}
